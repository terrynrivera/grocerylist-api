<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['type' => 'api', 'version' => '1.0', 'message' => 'API TEST'], 202,
            [
                'Content-Type' => 'application/json',
                'Charset' => 'utf-8'
            ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
    /**
     * Get all of the categories from the DB 
     *
     * @return categories (JSON)
     */
    public function getCategories()
    {
        $data = ['message' => "Error", 'error' => true, 'response' => []];
        $categories = DB::select('select * from grocerylist_categories'); 
        if($categories) {
            $data = ['message' => "Categories successfully retrieved!", 'error' => false, 'response' => $categories];
        } else {
            $data['message'] = "Database error!";
        }
        return response()->json($data, 202,
        [ 
            'Content-Type' => 'application/json',
            'Charset' => 'utf-8'
        ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
    /**
     * Get the user grocery list or create it if it does not exist
     *
     * @return user grocery list data (JSON)
     */
    public function getUserGroceryList($listId = 0)
    {
        $data = ['message' => "Error", 'error' => true, 'response' => [], 'status' => 500];
        if($listId) {
            $result = DB::table('grocerylist_user_list AS glul')
                    ->select(['glud.checked', 'glul.list_name as listName', 'glul.list_id as listId', 'glud.id as pId','glud.category_id as catId', 'glud.name as productName', 'glud.description as productDescription'])
                    ->leftJoin('grocerylist_user_data AS glud', 'glud.list_id', '=', 'glul.list_id')
                    ->where('glul.list_id', $listId)
                    ->orderBy('glud.id')
                    ->get()->toArray();
            $list = (array) $result;
            if(empty($list)) {
                //Grocery List doesnt exist, so we need to create one
                $list = [['checked' => false, 'pId' => null, 'catId' => null, 'listId' => $listId, 'listTimestamp' => time(), 'listName' => "New Grocery List", 'productDescription' => null, "productName" => null]];
                DB::table('grocerylist_user_list')->insert(['list_id' => $listId, 'list_timestamp' => time(), 'list_name' => "New Grocery List"]);
            }
            $data = ['message' => "Grocery List successfully retrieved!", 'error' => false, 'response' => $list, 'status' => 202];
        } else {
            $data['message'] = "List ID is required";
        }
        return response()->json($data, $data['status'],
        [ 
            'Content-Type' => 'application/json',
            'Charset' => 'utf-8'
        ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
     /**
     * Add to user grocery list
     *
     * @return user grocery list data (JSON)
     */
    public function addToUserGroceryList(Request $request) {
        $data = ['message' => "Error", 'error' => true, 'response' => [], 'status' => 500];
        $error = false;
        $input = json_decode($request->getContent(), true);
        $listId = (key_exists('listId', $input) ? stripslashes(strip_tags($input['listId'])) : '');
        $catId = (key_exists('catId', $input) ? stripslashes(strip_tags($input['catId'])) : '');
        $pDesc = (key_exists('productDesc', $input) ? stripslashes(strip_tags($input['productDesc'])) : '');
        $pName = (key_exists('productName', $input) ? stripslashes(strip_tags($input['productName'])) : '');
        if(!$pName) {
            $data['message'] = "Product Name cannot be empty.";
            $error = true;
        }
        if(!$listId) {
            $data['message'] = "List ID cannot be empty.";
            $error = true;
        }elseif(strlen($listId) > 6 || strlen($listId) < 6 || !ctype_alnum($listId)) {
            $data['message'] = "List ID must be 6 Alphanumeric characters.";
            $error = true;
        } 
        if(!$error) {
            $pId = DB::table('grocerylist_user_data')->insertGetId(['category_id' => $catId, 'list_id' => $listId, 'name' => $pName, 'description' => $pDesc, 'checked' => 0]);
            $data['status'] = 202;
            $data['pId'] = $pId;
            $data['error'] = false;
            $data['message'] = "Product data added successfully!";
        }
        //strip_tags
        return response()->json($data, $data['status'],
        [ 
            'Content-Type' => 'application/json',
            'Charset' => 'utf-8'
        ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
     /**
     * Update grocery list
     *
     * @return user grocery list data (JSON)
     */
    public function updateUserGroceryList(Request $request) {
        $data = ['message' => "Error", 'error' => true, 'response' => [], 'status' => 500];
        $error = false;
        $input = json_decode($request->getContent(), true);
        $listId = (key_exists('listId', $input) ? stripslashes(strip_tags($input['listId'])) : '');
        $checked = (key_exists('checked', $input) ? ($input['checked']  ? 1 : 0 ): 0);
        $pId = (key_exists('pId', $input) ? (is_numeric($input['pId'])  ? $input['pId'] : 0 ): 0);
        if(!$listId) {
            $data['message'] = "List ID cannot be empty.";
            $error = true;
        }elseif(strlen(($listId)) > 6 || strlen(($listId)) < 6 || !ctype_alnum($listId)) {
            $data['message'] = "List ID must be 6 Alphanumeric characters.";
            $error = true;
        } 
        if(!$pId) {
            $data['message'] = "Product ID cannot be empty.";
            $error = true;
        }
        if(!$error) {
            DB::table('grocerylist_user_data')->where(['list_id'=> $listId, 'id' => $pId])->update(['checked' => $checked]);
            $data['status'] = 202;
            $data['error'] = false;
            $data['message'] = "Product data updated successfully!";
        }
        //strip_tags
        return response()->json($data, $data['status'],
        [ 
            'Content-Type' => 'application/json',
            'Charset' => 'utf-8'
        ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
       /**
     * Delete grocery list
     *
     * @return user grocery list data (JSON)
     */
    public function deleteUserGroceryList(Request $request) {
        $data = ['message' => "Error", 'error' => true, 'response' => [], 'status' => 500];
        $error = false;
        $input = json_decode($request->getContent(), true);
        $listId = (key_exists('listId', $input) ? stripslashes(strip_tags($input['listId'])) : '');
        $pId = (key_exists('pId', $input) ? (is_numeric($input['pId'])  ? $input['pId'] : 0 ): 0);
        if(!$listId) {
            $data['message'] = "List ID cannot be empty.";
            $error = true;
        }elseif(strlen(($listId)) > 6 || strlen(($listId)) < 6 || !ctype_alnum($listId)) {
            $data['message'] = "List ID must be 6 Alphanumeric characters.";
            $error = true;
        } 
        if(!$pId) {
            $data['message'] = "Product ID cannot be empty.";
            $error = true;
        }
        if(!$error) {
            DB::table('grocerylist_user_data')->where(['list_id'=> $listId, 'id' => $pId])->delete();
            $data['status'] = 202;
            $data['error'] = false;
            $data['message'] = "Product data updated successfully!";
        }
        //strip_tags
        return response()->json($data, $data['status'],
        [ 
            'Content-Type' => 'application/json',
            'Charset' => 'utf-8'
        ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
}
