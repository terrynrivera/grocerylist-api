<?php

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/ 

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', [ApiController::class, 'index']);
Route::get('/v1', [ApiController::class, 'index']);
Route::get('/v1/categories', [ApiController::class, 'getCategories']);
Route::put('/v1/grocery/list', [ApiController::class, 'addToUserGroceryList']);
Route::post('/v1/grocery/list', [ApiController::class, 'updateUserGroceryList']);
Route::delete('/v1/grocery/list', [ApiController::class, 'deleteUserGroceryList']);
Route::get('/v1/grocery/list/{id}', [ApiController::class, 'getUserGroceryList']);
