<?php

use Illuminate\Support\Facades\DB;

Route::get('/', function () {
  return redirect('/api');
});
